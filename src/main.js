import Vue from 'vue';
import vueCustomElement from 'vue-custom-element';
import SlickWebComponent from './components/SlickWebComponent';
import App from './App';

Vue.config.productionTip = false;

Vue.use(vueCustomElement);
Vue.customElement('slick-carousel', SlickWebComponent, {
    shadow: false,
});

if(process.env.NODE_ENV === 'development'){
    new Vue({
        template: '<App/>',
        components: {App},
    }).$mount('#app');
}
